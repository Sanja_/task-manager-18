package ru.karamyshev.taskmanager.command.task;

import ru.karamyshev.taskmanager.api.service.IAuthService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.command.AbstractCommand;

public class TaskClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-tskclr";
    }

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        IAuthService authService = serviceLocator.getAuthService();
        ITaskService taskService = serviceLocator.getTaskService();
        final String userId = authService.getUserId();
        System.out.println("[CLEAR TASKS]");
        taskService.clear(userId);
        System.out.println("[OK]");
    }

}

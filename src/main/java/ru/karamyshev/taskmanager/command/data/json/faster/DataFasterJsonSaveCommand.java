package ru.karamyshev.taskmanager.command.data.json.faster;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.dto.Domain;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;

public class DataFasterJsonSaveCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-fs-json-save";
    }

    @Override
    public String description() {
        return "Save data from json file.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA JSON SAVE]");
        final Domain domain = getDomain();

        final File file = new File(FILE_FAST_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final ObjectMapper objectMapper = new ObjectMapper();
        final String json = objectMapper.writerWithDefaultPrettyPrinter()
                .writeValueAsString(domain);

        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

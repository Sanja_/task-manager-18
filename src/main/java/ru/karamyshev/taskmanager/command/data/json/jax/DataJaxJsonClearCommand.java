package ru.karamyshev.taskmanager.command.data.json.jax;

import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;

public class DataJaxJsonClearCommand extends AbstractDataCommand implements Serializable {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-jax-json-clear";
    }

    @Override
    public String description() {
        return "Remove json(jax-b) file.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[REMOVE JSON(JAX-V) FILE]");
        final File file = new File(FILE_JAX_JSON);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

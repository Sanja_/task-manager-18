package ru.karamyshev.taskmanager.command.data.xml.faster;

import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;

public class DataFasterXmlClearCommand extends AbstractDataCommand implements Serializable {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-fs-xml-clear";
    }

    @Override
    public String description() {
        return "Remove xml(faster) file.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[REMOVE XML(FASTER) FILE]");
        final File file = new File(FILE_FAST_XML);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

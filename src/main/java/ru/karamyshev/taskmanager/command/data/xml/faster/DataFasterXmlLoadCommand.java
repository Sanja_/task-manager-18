package ru.karamyshev.taskmanager.command.data.xml.faster;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.dto.Domain;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataFasterXmlLoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-fs-xml-load";
    }

    @Override
    public String description() {
        return "Save data to base64 file.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA BASE64 LOAD]");

        final String data = new String(Files.readAllBytes(Paths.get(FILE_FAST_XML)));

        final ObjectMapper objectMapper = new XmlMapper();
        final Domain domain = (Domain) objectMapper.readValue(data, Domain.class);

        setDomain(domain);
        System.out.println("[OK]");

        serviceLocator.getAuthService().logout();
        System.out.println("[YOU ARE LOGGED OUT]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

package ru.karamyshev.taskmanager.command.data.binary;

import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.dto.Domain;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DataBinaryLoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return "-dtbnld";
    }

    @Override
    public String name() {
        return "data-bin-load";
    }

    @Override
    public String description() {
        return "Save data to binary file.";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA BINARY LOAD]");
        final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        final Domain domain = (Domain) objectInputStream.readObject();

        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
        System.out.println("[OK]");

        serviceLocator.getAuthService().logout();
        System.out.println("[YOU ARE LOGGED OUT]");
    }

}

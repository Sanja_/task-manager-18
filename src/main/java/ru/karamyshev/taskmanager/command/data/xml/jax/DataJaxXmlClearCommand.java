package ru.karamyshev.taskmanager.command.data.xml.jax;

import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DataJaxXmlClearCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-jax-xml-clear";
    }

    @Override
    public String description() {
        return "Remove json(faster) file.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[REMOVE XML(JAX-B) FILE]");
        final File file = new File(FILE_JAX_XML);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

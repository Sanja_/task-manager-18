package ru.karamyshev.taskmanager.command.data.binary;

import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.dto.Domain;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

public class DataBinarySaveCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return "-dtbnsv";
    }

    @Override
    public String name() {
        return "data-bin-save";
    }

    @Override
    public String description() {
        return "Save data from binary file.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[DATA BINARY SAVE]");

        final Domain domain = getDomain();

        final File file = new File(FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        final FileOutputStream fileOutputStream = new FileOutputStream(file);
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();

        System.out.println("[OK]");
    }

}

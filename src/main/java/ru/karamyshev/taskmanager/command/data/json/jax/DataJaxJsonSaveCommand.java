package ru.karamyshev.taskmanager.command.data.json.jax;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.dto.Domain;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.nio.file.Files;

public class DataJaxJsonSaveCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-jax-json-save";
    }

    @Override
    public String description() {
        return "Save data from json(jax-b) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON(JAX-B) SAVE]");
        final Domain domain = getDomain();

        final File file = new File(FILE_JAX_JSON);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");

        JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Marshaller mar = jaxbContext.createMarshaller();

        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        mar.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);

        mar.marshal(domain, new File(FILE_JAX_JSON));

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

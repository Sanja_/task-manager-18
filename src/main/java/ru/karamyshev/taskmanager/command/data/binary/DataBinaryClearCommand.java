package ru.karamyshev.taskmanager.command.data.binary;

import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.enumerated.Role;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;

public class DataBinaryClearCommand extends AbstractDataCommand implements Serializable {

    @Override
    public String arg() {
        return "data-bin-clear";
    }

    @Override
    public String name() {
        return "-dtbnclr";
    }

    @Override
    public String description() {
        return "Remove binary file.";
    }

    @Override
    public void execute() throws IOException {
        System.out.println("[REMOVE BINARY FILE]");
        final File file = new File(FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

package ru.karamyshev.taskmanager.command.data.json.jax;

import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.dto.Domain;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class DataJaxJsonLoadCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-jax-json-load";
    }

    @Override
    public String description() {
        return "Load to json(jax-b) file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON(JAX-B) LOAD]");

        final File file = new File(FILE_JAX_JSON);

        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");

        JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        Unmarshaller un = jaxbContext.createUnmarshaller();

        un.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        un.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);

        Domain domain = (Domain) un.unmarshal(file);
        setDomain(domain);
        System.out.println("[OK]");

        serviceLocator.getAuthService().logout();
        System.out.println("[YOU ARE LOGGED OUT]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

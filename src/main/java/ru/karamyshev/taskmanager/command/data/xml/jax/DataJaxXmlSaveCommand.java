package ru.karamyshev.taskmanager.command.data.xml.jax;

import ru.karamyshev.taskmanager.command.AbstractDataCommand;
import ru.karamyshev.taskmanager.dto.Domain;
import ru.karamyshev.taskmanager.enumerated.Role;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class DataJaxXmlSaveCommand extends AbstractDataCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "data-jax-xml-save";
    }

    @Override
    public String description() {
        return "Save data from xml file.";
    }

    @Override
    public void execute() throws IOException, JAXBException {
        System.out.println("[DATA XML(JAX-B) SAVE]");
        final Domain domain = getDomain();

        final File file = new File(FILE_JAX_XML);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        JAXBContext context = JAXBContext.newInstance(Domain.class);
        Marshaller mar = context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        mar.marshal(domain, new File(FILE_JAX_XML));

        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}

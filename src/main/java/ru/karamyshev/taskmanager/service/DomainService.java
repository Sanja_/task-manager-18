package ru.karamyshev.taskmanager.service;

import ru.karamyshev.taskmanager.api.service.IDomainService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.dto.Domain;

public class DomainService implements IDomainService {

    private final IUserService userService;

    private final ITaskService taskService;

    private final IProjectService projectService;

    public DomainService(
            final IUserService userService,
            final ITaskService taskService,
            final IProjectService projectService
    ) {
        this.userService = userService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Override
    public void load(final Domain domain) {
        if (domain == null) return;
        projectService.load(domain.getProjects());
        taskService.load(domain.getTasks());
        userService.load(domain.getUsers());
    }

    @Override
    public void export(final Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.getProjectsList());
        domain.setTasks(taskService.getTasksList());
        domain.setUsers(userService.getUsersList());
    }

}

package ru.karamyshev.taskmanager.api.service;

import ru.karamyshev.taskmanager.dto.Domain;

public interface IDomainService {

    void load(Domain domain);

    void export(Domain domain);

}
